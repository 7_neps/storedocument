﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace StoreDocuments_
{
    public partial class Form1 : Form
    {
        List<tramite> _Listtramite = new List<tramite>();
        List<Keywords> _ListKeywords = new List<Keywords>();
        int PaginaActual = 0;
        int TotalPagina = 0;
        string RutaTemporal = "";
        SqlConnection conexion = new SqlConnection();

        public Form1()
        {
            InitializeComponent();
        }

        private void splitContainer1_SplitterMoved(object sender, SplitterEventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button3_Click(object sender, EventArgs e)
        {

            if (this.WindowState == FormWindowState.Maximized)
            {
                this.WindowState = FormWindowState.Normal;
                this.SC_Cuerpo.Width = 903;
                Btn_Mini.Text = "Maximizar";
            }
            else
            {
                
                this.WindowState = FormWindowState.Maximized;
                this.SC_Cuerpo.Width = 903;
                Btn_Mini.Text = "Normal";
            }
        }

        private void Txt_Detail_number_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(char.IsNumber(e.KeyChar)) && (e.KeyChar != (char)Keys.Back))
            {
                if ((int)e.KeyChar == (int)Keys.Enter)
                {
                    button2_Click(null, null);
                }
                else
                {
                    e.Handled = true;
                    return;
                }
            }
            
        }

        private void button2_Click(object sender, EventArgs e)
        {
            lbl_info.Visible = false;
            if (Txt_Detail_number.Text != "")
            {
                if (Txt_Detail_number.Text.Length >= 9)
                {
                    
                    #region Busqueda en BD

                    Dictionary<string, object> parametros = new Dictionary<string, object>();
                    parametros.Add("@Value", Txt_Detail_number.Text);
                    DataTable resp = ExecuteSP(conexion, "[dbo].[EcatchDesktop_StoreDocuments]", parametros);

                    if (resp.Rows.Count > 0)
                    {

                        string Document_Id = resp.Rows[0][0].ToString();
                        ClassData.Document_id = Document_Id;
                        string Ruta = resp.Rows[0][1].ToString();
                        FileInfo InfoF = new FileInfo(Ruta);
                        string NombreArchivo = InfoF.Name;
                        if (File.Exists(Ruta))
                        {
                            #region Recorrer Imagen

                            SC_Cuerpo.Visible = true;
                            Split(Ruta, RutaTemporal, NombreArchivo);
                            
                           // DrawKeywords(Document_Id);
                            DataTable Res = GetValues(Document_Id);
                            if (Res.Rows.Count == 1 )
                            {
                                // si trae keyword los actualiza
                               string Prefijo_de_Factura = Res.Rows[0][1].ToString();
                               string Numero_de_Factura = Res.Rows[0][2].ToString();
                               string Total_Factura= Res.Rows[0][3].ToString();
                               string Sub_Total= Res.Rows[0][4].ToString();
                               string iva= Res.Rows[0][5].ToString();
                               string Co_Pago = Res.Rows[0][6].ToString();
                               string Cuota_Moderadora = Res.Rows[0][7].ToString();
                               string fecha_Factura = Res.Rows[0][8].ToString();
                               string Tipo_Identificacion_Paciente = Res.Rows[0][9].ToString();
                               string Numero_Identificacion_Paciente = Res.Rows[0][10].ToString();
                               string Fecha_Atencion = Res.Rows[0][11].ToString();
                               string codigo_IPS = Res.Rows[0][12].ToString();
                               string caja = Res.Rows[0][13].ToString();
                               string radication_number = Res.Rows[0][14].ToString();

                               
                               txt_Prefijo.Text = Prefijo_de_Factura;
                               txt_Numero.Text = Numero_de_Factura;
                               txt_Total.Text = Total_Factura;
                               txt_Subtotal.Text = Sub_Total;
                               txt_iva.Text = iva;
                               txt_CoPago.Text = Co_Pago;
                               txt_CuotaM.Text = Cuota_Moderadora;
                               Mask_Fecha.Text = fecha_Factura;
                               cb_TipoId.SelectedItem = Tipo_Identificacion_Paciente;
                               txt_NumeroID.Text = Numero_Identificacion_Paciente;
                               Mask_FechaAtencion.Text = Fecha_Atencion;
                               txt_CodigoIps.Text = codigo_IPS;
                               txt_Caja.Text = caja;
                               txt_Radicacion.Text = radication_number;
                               Btn_Guardar.Visible = true;
                            }

                            ViewImage(0);
                            lblDocumentos.Text = "Doc.: " + (PaginaActual + 1) + "/" + TotalPagina.ToString();
                            Pn_Controles.Visible = true;
                            #endregion
                        }
                        else
                        {
                            lbl_info.Visible = true;
                            lbl_info.Text = "No es posible accede a al imagen.";
                        }

                    }
                    else
                    {
                        lbl_info.Visible = true;
                        lbl_info.Text = "El Detail Nunmber no arroja resultados, Por favor verifique que este bien escrito.";
                    }
                    #endregion

                }
                else
                {
                    lbl_info.Visible = true;
                    lbl_info.Text = "El Detail Number debe tener minimo 10 caracteres";
                }
            }
            else
            {
                lbl_info.Visible = true;
                lbl_info.Text = "Es necesario escribir el Detail_number";
            }
        }

        private DataTable GetValues(string Document_Id)
        {
            try
            {

                Dictionary<string, object> parametros = new Dictionary<string, object>();
                parametros.Add("@document_id", Document_Id);
                DataTable resp = ExecuteSP(conexion, "[dbo].[EcatchDesktop_StoreDocuments_GetValues]", parametros);
                return resp;
            }catch(Exception ex) {  return null;}
        }

        private void ViewImage(int Cantidad)
        {
            try
            {
                int Contador = 0;
                foreach (tramite datos in _Listtramite)
                {
                    if (Contador == Cantidad)
                    {

                        System.IO.FileStream fs = new System.IO.FileStream(datos.Ruta, FileMode.Open, FileAccess.Read);
                        pictureBox1.Image = System.Drawing.Image.FromStream(fs);
                        fs.Dispose();
                        break;
                    }
                    else
                    {
                        Contador++;
                    }
                }
            }
            catch (Exception ex)
            { }
        }

        private void Split(string pstrInputFilePath, string pstrOutputPath, string NombreArchivo)
        {

            int activePage = 0;
            int pages;

            System.Drawing.Image image = System.Drawing.Image.FromFile(pstrInputFilePath);
            string Tramite = Txt_Detail_number.Text;
            string Rtadestino = pstrOutputPath + Tramite + "\\";
            if (!Directory.Exists(Rtadestino)) { Directory.CreateDirectory(Rtadestino); }
            pages = image.GetFrameCount(FrameDimension.Page);
            if (TotalPagina <= pages) { pages = TotalPagina; }

            for (int index = 0; index < pages; index++)
            {

                image.SelectActiveFrame(FrameDimension.Page, index);
                image.Save(Rtadestino + activePage.ToString() + ".tif");
                _Listtramite.Add(new tramite(Tramite, index, Rtadestino + activePage.ToString() + ".tif"));
                activePage = index + 1;
            }

            image.Dispose();
        } 

        private void btnDocSig_Click(object sender, EventArgs e)
        {
            lbl_info.Visible = false;
            if (PaginaActual < TotalPagina-1)
            {
                PaginaActual = PaginaActual + 1;
                ViewImage(PaginaActual);
                lblDocumentos.Text = "Doc.: " + (PaginaActual + 1) + "/" + TotalPagina.ToString();
            }
            else
            {
                lbl_info.Visible = true;
                lbl_info.Text = "Esta es la ultima pagina";
            }
        }

        private void btnDocAnt_Click(object sender, EventArgs e)
        {
            lbl_info.Visible = false;
            if (PaginaActual - 1 >= 0)
            {

                PaginaActual = PaginaActual - 1;
                ViewImage(PaginaActual);
                lblDocumentos.Text = "Doc.: " + (PaginaActual + 1) + "/" + TotalPagina.ToString();
            }
            else
            {
                lbl_info.Visible = true;
                lbl_info.Text = "Esta es la primera pagina";
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            try
            {
                TotalPagina = int.Parse(System.Configuration.ConfigurationManager.AppSettings["TotalPagina"]);
                RutaTemporal = System.Configuration.ConfigurationManager.AppSettings["RutaTemp"];
                GetConexion();
            }catch(Exception ex)
            {
            }
        }

        private void button3_Click_1(object sender, EventArgs e)
        {
            if (DialogResult.OK == MessageBox.Show("Esta seguro que desea limpiar los valores", "Informacion", MessageBoxButtons.OKCancel))
            {
                try
                {
                    Directory.Delete(RutaTemporal,true);
                    Directory.CreateDirectory(RutaTemporal);
                    SC_Cuerpo.Visible = false;
                    Txt_Detail_number.Focus();
                    Txt_Detail_number.Text = "";
                    lbl_info.Text = "";
                    Pn_Controles.Visible = false;
                    Btn_Guardar.Visible = false;
                }
                catch (Exception ex) { }

            }
        }

        private void GetConexion()
        {
            string CON = ConfigurationManager.ConnectionStrings["ConexionDB"].ConnectionString;
            conexion = new SqlConnection(CON);
            conexion.Open();
        }

        private DataTable ExecuteSP(SqlConnection conexion, string storeProcedure, Dictionary<string, object> parametro, bool isParametro = false)
        {

            SqlCommand comando = new SqlCommand(storeProcedure, conexion);
            foreach (KeyValuePair<string, object> aux in parametro)
                comando.Parameters.Add(new SqlParameter(aux.Key, aux.Value));
            comando.CommandType = CommandType.StoredProcedure;
            comando.CommandTimeout = 0;
            SqlDataReader objRead = comando.ExecuteReader();
            DataTable dt = new DataTable();
            dt.Load(objRead);
            GC.Collect();
            return dt;
        }

        public class tramite
        {

            public tramite(string tramite, int pagina, string ruta)
            {
                this.Tramite = tramite;
                this.Pagina = pagina;
                this.Ruta = ruta;
            }
            public string Tramite { get; set; }
            public string Ruta { get; set; }
            public int Pagina { get; set; }


        }

        public class Keywords
        {

            public Keywords(int id, string Nombre, string Document_Id, string Valor)
            {
                this.id = id;
                this.Nombre = Nombre;
                this.Document_Id = Document_Id;
                this.Valor = Valor;
            }
            public string Valor { get; set; }
            public string Document_Id { get; set; }
            public string Nombre { get; set; }
            public int id { get; set; }

        }

        private void Btn_Guardar_Click(object sender, EventArgs e)
        {
            try
            {
                if (DialogResult.OK == MessageBox.Show("Esta seguro que desea Guardar esta informacion", "Informacion", MessageBoxButtons.OKCancel))
                {

                    string Prefijo_de_Factura = txt_Prefijo.Text;
                    string Numero_de_Factura = txt_Numero.Text;
                    string Total_Factura = txt_Total.Text;
                    string Sub_Total = txt_Subtotal.Text;
                    string iva = txt_iva.Text;
                    string Co_Pago = txt_CoPago.Text;
                    string Cuota_Moderadora = txt_CuotaM.Text;
                    string fecha_Factura = Mask_Fecha.Text;
                    string Tipo_Identificacion_Paciente = cb_TipoId.SelectedText;
                    string Numero_Identificacion_Paciente = txt_NumeroID.Text;
                    string Fecha_Atencion = Mask_FechaAtencion.Text;
                    string codigo_IPS = txt_CodigoIps.Text;
                    string caja = txt_Caja.Text;
                    string radication_number = txt_Radicacion.Text;

                    Dictionary<string, object> parametros = new Dictionary<string, object>();
                    parametros.Add("@document_id", ClassData.Document_id);
                    parametros.Add("@Prefijo_de_Factura", Prefijo_de_Factura);
                    parametros.Add("@Numero_de_Factura", Numero_de_Factura);
                    parametros.Add("@Total_Factura", Total_Factura);
                    parametros.Add("@Sub_Total", Sub_Total);
                    parametros.Add("@iva", iva);
                    parametros.Add("@Co_Pago", Co_Pago);
                    parametros.Add("@Cuota_Moderadora", Cuota_Moderadora);
                    parametros.Add("@fecha_Factura", fecha_Factura);
                    parametros.Add("@Tipo_Identificacion_Paciente", Tipo_Identificacion_Paciente);
                    parametros.Add("@Numero_Identificacion_Paciente", Numero_Identificacion_Paciente);
                    parametros.Add("@Fecha_Atencion", Fecha_Atencion);
                    parametros.Add("@codigo_IPS", codigo_IPS);
                    parametros.Add("@caja", caja);
                    parametros.Add("@radication_number", radication_number);
                    DataTable resp = ExecuteSP(conexion, "[dbo].[EcatchDesktop_StoreDocuments_SetValues]", parametros);

                    try
                    {
                        Directory.Delete(RutaTemporal, true);
                        Directory.CreateDirectory(RutaTemporal);
                        SC_Cuerpo.Visible = false;
                        Txt_Detail_number.Focus();
                        Txt_Detail_number.Text = "";
                        lbl_info.Text = "";
                        Pn_Controles.Visible = false;
                        ClassData.Document_id = "";
                        Btn_Guardar.Visible = false;
                    }
                    catch (Exception ex) { }

                }
            }
            catch (Exception) { }
        }

    }
}
