﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace StoreDocuments_
{
    public partial class Autenticacion : Form
    {
        SqlConnection conexion = new SqlConnection();

        public Autenticacion()
        {
            InitializeComponent();
        }

        private void btn_salir_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btn_Ingresar_Click(object sender, EventArgs e)
        {
            lbl_mensaje.Visible = false;
            Dictionary<string, object> parametros = new Dictionary<string, object>();
            parametros.Add("@usuario", txt_Usuario.Text);
            DataTable dt = ExecuteSP(conexion, "[dbo].[EcatchDesktop_StoreDocuments_Login]", parametros);

           // if (txt_Usuario.Text != "")
            if (dt.Rows[0][0].ToString() == "1")
            {
                ClassData.usuario = txt_Usuario.Text;
                ClassData.Autenticacion = 1;
                this.Close();
            }
            else
            {
                lbl_mensaje.Text = "La combinacion Usuario/contraseña no coincide";
                lbl_mensaje.Visible = true;
                txt_Contrasena.Text = "";
                txt_Usuario.Text = "";
                txt_Usuario.Focus();
            }
        }



        #region Metodos Basicos
 
        private DataTable ExecuteSP(SqlConnection conexion, string storeProcedure, Dictionary<string, object> parametro, bool isParametro = false)
        {

            SqlCommand comando = new SqlCommand(storeProcedure, conexion);
            foreach (KeyValuePair<string, object> aux in parametro)
                comando.Parameters.Add(new SqlParameter(aux.Key, aux.Value));
            comando.CommandType = CommandType.StoredProcedure;
            comando.CommandTimeout = 0;
            SqlDataReader objRead = comando.ExecuteReader();
            DataTable dt = new DataTable();
            dt.Load(objRead);
            GC.Collect();
            return dt;
        }

        private void GetConexion()
        {
           

            //string Server = @".\sqlexpress";
            //string DatabaseName = "FORMS_E3";
            //string UserName = "sa";
            //string Password = "assenda.2012";

            //SqlConnection sConnection = new SqlConnection();
            //sConnection.ConnectionString = @"Data Source=" + Server + ";Initial Catalog=" + DatabaseName + ";User ID=" + UserName + ";Password=" + Password + ";Connect Timeout=60000;MultipleActiveResultSets=True";

            //return sConnection;

            string CON = ConfigurationManager.ConnectionStrings["ConexionDB"].ConnectionString;
            conexion = new SqlConnection(CON);
            conexion.Open();
        }

 
        #endregion

        private void Autenticacion_Load(object sender, EventArgs e)
        {
            GetConexion();
        }
    }
}
