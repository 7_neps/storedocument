﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace StoreDocuments_
{
    public partial class Principal : Form
    {
        public Principal()
        {
            InitializeComponent();
        }



        private void Principal_Load(object sender, EventArgs e)
        {
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;

            if (this.MdiChildren.Length <= 0)
            {
                Autenticacion vl_objFrmLogin = new Autenticacion();
                vl_objFrmLogin.ShowDialog(this);

                if (ClassData.Autenticacion == 0)
                {
                    this.Close();
                }
                else
                {
                    txt_Verison.Text = "Version 1.0.0.1";
                    Txt_Usuario.Text = ClassData.usuario;
                }
            }
        }

        private void salirToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void recorteManualToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (this.MdiChildren.Length <= 0)
            {
                Form1 Obj = new Form1();
                Obj.MdiParent = this;
                Obj.Show();


            }
        }

   

        private void cerrarSessionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (this.MdiChildren.Length <= 0)
            {
                ClassData.Autenticacion = 0;
                Autenticacion vl_objFrmLogin = new Autenticacion();
                vl_objFrmLogin.ShowDialog(this);

                if (ClassData.Autenticacion == 0)
                {
                    this.Close();
                }
                else
                {
                    txt_Verison.Text = "Version 1.0.0.1";
                    Txt_Usuario.Text = ClassData.usuario;
                }
            }
        }

        private void storeDocumentsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (this.MdiChildren.Length <= 0)
            {
                Form1 Obj = new Form1();
                Obj.MdiParent = this;
                Obj.Show();


            }
        }



    }
}
