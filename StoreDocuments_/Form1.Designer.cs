﻿namespace StoreDocuments_
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.Pn_Controles = new System.Windows.Forms.Panel();
            this.btnDocAnt = new System.Windows.Forms.Button();
            this.lblDocumentos = new System.Windows.Forms.Label();
            this.btnDocSig = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.Txt_Detail_number = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.SC_Cuerpo = new System.Windows.Forms.SplitContainer();
            this.label16 = new System.Windows.Forms.Label();
            this.txt_Prefijo = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.txt_Numero = new System.Windows.Forms.TextBox();
            this.Mask_Fecha = new System.Windows.Forms.MaskedTextBox();
            this.Mask_FechaAtencion = new System.Windows.Forms.MaskedTextBox();
            this.cb_TipoId = new System.Windows.Forms.ComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txt_Radicacion = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.txt_Caja = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.txt_CodigoIps = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.txt_NumeroID = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.txt_CuotaM = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txt_CoPago = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txt_iva = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txt_Subtotal = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txt_Total = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.button1 = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.Btn_Guardar = new System.Windows.Forms.Button();
            this.lbl_info = new System.Windows.Forms.Label();
            this.Btn_Mini = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.Pn_Controles.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SC_Cuerpo)).BeginInit();
            this.SC_Cuerpo.Panel1.SuspendLayout();
            this.SC_Cuerpo.Panel2.SuspendLayout();
            this.SC_Cuerpo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.Pn_Controles);
            this.groupBox1.Controls.Add(this.button3);
            this.groupBox1.Controls.Add(this.button2);
            this.groupBox1.Controls.Add(this.Txt_Detail_number);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(825, 45);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            // 
            // Pn_Controles
            // 
            this.Pn_Controles.Controls.Add(this.btnDocAnt);
            this.Pn_Controles.Controls.Add(this.lblDocumentos);
            this.Pn_Controles.Controls.Add(this.btnDocSig);
            this.Pn_Controles.Location = new System.Drawing.Point(511, 8);
            this.Pn_Controles.Name = "Pn_Controles";
            this.Pn_Controles.Size = new System.Drawing.Size(179, 34);
            this.Pn_Controles.TabIndex = 63;
            this.Pn_Controles.Visible = false;
            // 
            // btnDocAnt
            // 
            this.btnDocAnt.BackColor = System.Drawing.Color.Transparent;
            this.btnDocAnt.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnDocAnt.ForeColor = System.Drawing.Color.White;
            this.btnDocAnt.Image = global::StoreDocuments_.Properties.Resources.backward2;
            this.btnDocAnt.Location = new System.Drawing.Point(14, 6);
            this.btnDocAnt.Name = "btnDocAnt";
            this.btnDocAnt.Size = new System.Drawing.Size(34, 24);
            this.btnDocAnt.TabIndex = 60;
            this.btnDocAnt.UseVisualStyleBackColor = false;
            this.btnDocAnt.Click += new System.EventHandler(this.btnDocAnt_Click);
            // 
            // lblDocumentos
            // 
            this.lblDocumentos.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lblDocumentos.Location = new System.Drawing.Point(54, 17);
            this.lblDocumentos.Name = "lblDocumentos";
            this.lblDocumentos.Size = new System.Drawing.Size(66, 13);
            this.lblDocumentos.TabIndex = 62;
            this.lblDocumentos.Text = "Doc.: 0/0";
            this.lblDocumentos.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnDocSig
            // 
            this.btnDocSig.BackColor = System.Drawing.Color.Transparent;
            this.btnDocSig.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnDocSig.ForeColor = System.Drawing.Color.White;
            this.btnDocSig.Image = global::StoreDocuments_.Properties.Resources.forward3;
            this.btnDocSig.Location = new System.Drawing.Point(126, 6);
            this.btnDocSig.Name = "btnDocSig";
            this.btnDocSig.Size = new System.Drawing.Size(34, 24);
            this.btnDocSig.TabIndex = 61;
            this.btnDocSig.UseVisualStyleBackColor = false;
            this.btnDocSig.Click += new System.EventHandler(this.btnDocSig_Click);
            // 
            // button3
            // 
            this.button3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.button3.BackColor = System.Drawing.Color.RoyalBlue;
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button3.ForeColor = System.Drawing.Color.White;
            this.button3.Location = new System.Drawing.Point(702, 14);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(54, 23);
            this.button3.TabIndex = 2;
            this.button3.Text = "Limpiar";
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Click += new System.EventHandler(this.button3_Click_1);
            // 
            // button2
            // 
            this.button2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.button2.BackColor = System.Drawing.Color.RoyalBlue;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.ForeColor = System.Drawing.Color.White;
            this.button2.Location = new System.Drawing.Point(762, 14);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(54, 23);
            this.button2.TabIndex = 1;
            this.button2.Text = "Buscar";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // Txt_Detail_number
            // 
            this.Txt_Detail_number.Location = new System.Drawing.Point(123, 14);
            this.Txt_Detail_number.MaxLength = 12;
            this.Txt_Detail_number.Name = "Txt_Detail_number";
            this.Txt_Detail_number.Size = new System.Drawing.Size(100, 20);
            this.Txt_Detail_number.TabIndex = 1;
            this.Txt_Detail_number.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Txt_Detail_number_KeyPress);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(14, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(103, 18);
            this.label1.TabIndex = 0;
            this.label1.Text = "Detail_number";
            // 
            // SC_Cuerpo
            // 
            this.SC_Cuerpo.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.SC_Cuerpo.Location = new System.Drawing.Point(2, 47);
            this.SC_Cuerpo.Name = "SC_Cuerpo";
            // 
            // SC_Cuerpo.Panel1
            // 
            this.SC_Cuerpo.Panel1.AutoScroll = true;
            this.SC_Cuerpo.Panel1.BackColor = System.Drawing.Color.Silver;
            this.SC_Cuerpo.Panel1.Controls.Add(this.label16);
            this.SC_Cuerpo.Panel1.Controls.Add(this.txt_Prefijo);
            this.SC_Cuerpo.Panel1.Controls.Add(this.label17);
            this.SC_Cuerpo.Panel1.Controls.Add(this.txt_Numero);
            this.SC_Cuerpo.Panel1.Controls.Add(this.Mask_Fecha);
            this.SC_Cuerpo.Panel1.Controls.Add(this.Mask_FechaAtencion);
            this.SC_Cuerpo.Panel1.Controls.Add(this.cb_TipoId);
            this.SC_Cuerpo.Panel1.Controls.Add(this.label13);
            this.SC_Cuerpo.Panel1.Controls.Add(this.txt_Radicacion);
            this.SC_Cuerpo.Panel1.Controls.Add(this.label14);
            this.SC_Cuerpo.Panel1.Controls.Add(this.txt_Caja);
            this.SC_Cuerpo.Panel1.Controls.Add(this.label15);
            this.SC_Cuerpo.Panel1.Controls.Add(this.txt_CodigoIps);
            this.SC_Cuerpo.Panel1.Controls.Add(this.label8);
            this.SC_Cuerpo.Panel1.Controls.Add(this.label9);
            this.SC_Cuerpo.Panel1.Controls.Add(this.txt_NumeroID);
            this.SC_Cuerpo.Panel1.Controls.Add(this.label10);
            this.SC_Cuerpo.Panel1.Controls.Add(this.label11);
            this.SC_Cuerpo.Panel1.Controls.Add(this.label12);
            this.SC_Cuerpo.Panel1.Controls.Add(this.txt_CuotaM);
            this.SC_Cuerpo.Panel1.Controls.Add(this.label5);
            this.SC_Cuerpo.Panel1.Controls.Add(this.txt_CoPago);
            this.SC_Cuerpo.Panel1.Controls.Add(this.label6);
            this.SC_Cuerpo.Panel1.Controls.Add(this.txt_iva);
            this.SC_Cuerpo.Panel1.Controls.Add(this.label7);
            this.SC_Cuerpo.Panel1.Controls.Add(this.txt_Subtotal);
            this.SC_Cuerpo.Panel1.Controls.Add(this.label4);
            this.SC_Cuerpo.Panel1.Controls.Add(this.txt_Total);
            this.SC_Cuerpo.Panel1.Controls.Add(this.label3);
            this.SC_Cuerpo.Panel1.Controls.Add(this.label2);
            // 
            // SC_Cuerpo.Panel2
            // 
            this.SC_Cuerpo.Panel2.Controls.Add(this.pictureBox1);
            this.SC_Cuerpo.Size = new System.Drawing.Size(823, 353);
            this.SC_Cuerpo.SplitterDistance = 221;
            this.SC_Cuerpo.TabIndex = 1;
            this.SC_Cuerpo.Visible = false;
            this.SC_Cuerpo.SplitterMoved += new System.Windows.Forms.SplitterEventHandler(this.splitContainer1_SplitterMoved);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(8, 54);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(104, 18);
            this.label16.TabIndex = 34;
            this.label16.Text = "Prefijo Factura";
            // 
            // txt_Prefijo
            // 
            this.txt_Prefijo.Location = new System.Drawing.Point(8, 75);
            this.txt_Prefijo.Name = "txt_Prefijo";
            this.txt_Prefijo.Size = new System.Drawing.Size(183, 20);
            this.txt_Prefijo.TabIndex = 35;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(8, 10);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(116, 18);
            this.label17.TabIndex = 32;
            this.label17.Text = "Numero Factura";
            // 
            // txt_Numero
            // 
            this.txt_Numero.Location = new System.Drawing.Point(8, 31);
            this.txt_Numero.Name = "txt_Numero";
            this.txt_Numero.Size = new System.Drawing.Size(183, 20);
            this.txt_Numero.TabIndex = 33;
            // 
            // Mask_Fecha
            // 
            this.Mask_Fecha.Location = new System.Drawing.Point(11, 356);
            this.Mask_Fecha.Mask = "00/00/0000";
            this.Mask_Fecha.Name = "Mask_Fecha";
            this.Mask_Fecha.Size = new System.Drawing.Size(186, 20);
            this.Mask_Fecha.TabIndex = 31;
            this.Mask_Fecha.ValidatingType = typeof(System.DateTime);
            // 
            // Mask_FechaAtencion
            // 
            this.Mask_FechaAtencion.Location = new System.Drawing.Point(11, 498);
            this.Mask_FechaAtencion.Mask = "00/00/0000";
            this.Mask_FechaAtencion.Name = "Mask_FechaAtencion";
            this.Mask_FechaAtencion.Size = new System.Drawing.Size(186, 20);
            this.Mask_FechaAtencion.TabIndex = 30;
            this.Mask_FechaAtencion.ValidatingType = typeof(System.DateTime);
            // 
            // cb_TipoId
            // 
            this.cb_TipoId.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_TipoId.FormattingEnabled = true;
            this.cb_TipoId.Items.AddRange(new object[] {
            "TI",
            "CC",
            "CE",
            "RC",
            "PS",
            "NU",
            "MS",
            "CD",
            "PA",
            "CN",
            "NV",
            "SC",
            "AS",
            "PE"});
            this.cb_TipoId.Location = new System.Drawing.Point(11, 404);
            this.cb_TipoId.Name = "cb_TipoId";
            this.cb_TipoId.Size = new System.Drawing.Size(180, 21);
            this.cb_TipoId.TabIndex = 29;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(11, 616);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(135, 18);
            this.label13.TabIndex = 27;
            this.label13.Text = "Radication Number";
            // 
            // txt_Radicacion
            // 
            this.txt_Radicacion.Location = new System.Drawing.Point(11, 636);
            this.txt_Radicacion.Name = "txt_Radicacion";
            this.txt_Radicacion.Size = new System.Drawing.Size(183, 20);
            this.txt_Radicacion.TabIndex = 28;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(11, 566);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(38, 18);
            this.label14.TabIndex = 25;
            this.label14.Text = "Caja";
            // 
            // txt_Caja
            // 
            this.txt_Caja.Location = new System.Drawing.Point(11, 588);
            this.txt_Caja.Name = "txt_Caja";
            this.txt_Caja.Size = new System.Drawing.Size(183, 20);
            this.txt_Caja.TabIndex = 26;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(11, 520);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(79, 18);
            this.label15.TabIndex = 23;
            this.label15.Text = "Codigo Ips";
            // 
            // txt_CodigoIps
            // 
            this.txt_CodigoIps.Location = new System.Drawing.Point(11, 539);
            this.txt_CodigoIps.Name = "txt_CodigoIps";
            this.txt_CodigoIps.Size = new System.Drawing.Size(183, 20);
            this.txt_CodigoIps.TabIndex = 24;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(11, 476);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(165, 18);
            this.label8.TabIndex = 21;
            this.label8.Text = "Fecha Atencion (D/M/Y)";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(11, 428);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(212, 18);
            this.label9.TabIndex = 19;
            this.label9.Text = "Numero Identificacion Paciente";
            // 
            // txt_NumeroID
            // 
            this.txt_NumeroID.Location = new System.Drawing.Point(11, 449);
            this.txt_NumeroID.Name = "txt_NumeroID";
            this.txt_NumeroID.Size = new System.Drawing.Size(183, 20);
            this.txt_NumeroID.TabIndex = 20;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(11, 379);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(187, 18);
            this.label10.TabIndex = 17;
            this.label10.Text = "Tipo Identificacion Paciente";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(11, 335);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(103, 18);
            this.label11.TabIndex = 15;
            this.label11.Text = "Fecha Factura";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(11, 287);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(133, 18);
            this.label12.TabIndex = 13;
            this.label12.Text = "Cuota Moderadora";
            // 
            // txt_CuotaM
            // 
            this.txt_CuotaM.Location = new System.Drawing.Point(11, 308);
            this.txt_CuotaM.Name = "txt_CuotaM";
            this.txt_CuotaM.Size = new System.Drawing.Size(183, 20);
            this.txt_CuotaM.TabIndex = 14;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(11, 241);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(67, 18);
            this.label5.TabIndex = 11;
            this.label5.Text = "Co Pago";
            // 
            // txt_CoPago
            // 
            this.txt_CoPago.Location = new System.Drawing.Point(11, 264);
            this.txt_CoPago.Name = "txt_CoPago";
            this.txt_CoPago.Size = new System.Drawing.Size(183, 20);
            this.txt_CoPago.TabIndex = 12;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(11, 194);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(26, 18);
            this.label6.TabIndex = 9;
            this.label6.Text = "iva";
            // 
            // txt_iva
            // 
            this.txt_iva.Location = new System.Drawing.Point(11, 216);
            this.txt_iva.Name = "txt_iva";
            this.txt_iva.Size = new System.Drawing.Size(183, 20);
            this.txt_iva.TabIndex = 10;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(11, 146);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(71, 18);
            this.label7.TabIndex = 7;
            this.label7.Text = "Sub Total";
            // 
            // txt_Subtotal
            // 
            this.txt_Subtotal.Location = new System.Drawing.Point(11, 167);
            this.txt_Subtotal.Name = "txt_Subtotal";
            this.txt_Subtotal.Size = new System.Drawing.Size(183, 20);
            this.txt_Subtotal.TabIndex = 8;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(11, 102);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(115, 18);
            this.label4.TabIndex = 5;
            this.label4.Text = "Total de Factura";
            // 
            // txt_Total
            // 
            this.txt_Total.Location = new System.Drawing.Point(11, 123);
            this.txt_Total.Name = "txt_Total";
            this.txt_Total.Size = new System.Drawing.Size(183, 20);
            this.txt_Total.TabIndex = 6;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(10, -190);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(136, 18);
            this.label3.TabIndex = 3;
            this.label3.Text = "Numero de Factura";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(9, -235);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(124, 18);
            this.label2.TabIndex = 2;
            this.label2.Text = "Prefijo de Factura";
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.DimGray;
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(598, 353);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.button1.BackColor = System.Drawing.Color.RoyalBlue;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.ForeColor = System.Drawing.Color.White;
            this.button1.Location = new System.Drawing.Point(747, 4);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 22);
            this.button1.TabIndex = 0;
            this.button1.Text = "Salir";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.Btn_Guardar);
            this.panel1.Controls.Add(this.lbl_info);
            this.panel1.Controls.Add(this.Btn_Mini);
            this.panel1.Controls.Add(this.button1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 403);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(825, 30);
            this.panel1.TabIndex = 2;
            // 
            // Btn_Guardar
            // 
            this.Btn_Guardar.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Btn_Guardar.BackColor = System.Drawing.Color.RoyalBlue;
            this.Btn_Guardar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Btn_Guardar.ForeColor = System.Drawing.Color.White;
            this.Btn_Guardar.Location = new System.Drawing.Point(499, 3);
            this.Btn_Guardar.Name = "Btn_Guardar";
            this.Btn_Guardar.Size = new System.Drawing.Size(164, 23);
            this.Btn_Guardar.TabIndex = 3;
            this.Btn_Guardar.Text = "Guardar";
            this.Btn_Guardar.UseVisualStyleBackColor = false;
            this.Btn_Guardar.Visible = false;
            this.Btn_Guardar.Click += new System.EventHandler(this.Btn_Guardar_Click);
            // 
            // lbl_info
            // 
            this.lbl_info.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.lbl_info.AutoSize = true;
            this.lbl_info.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_info.ForeColor = System.Drawing.Color.Red;
            this.lbl_info.Location = new System.Drawing.Point(6, 6);
            this.lbl_info.Name = "lbl_info";
            this.lbl_info.Size = new System.Drawing.Size(14, 18);
            this.lbl_info.TabIndex = 2;
            this.lbl_info.Text = "*";
            this.lbl_info.Visible = false;
            // 
            // Btn_Mini
            // 
            this.Btn_Mini.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Btn_Mini.BackColor = System.Drawing.Color.RoyalBlue;
            this.Btn_Mini.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Btn_Mini.ForeColor = System.Drawing.Color.White;
            this.Btn_Mini.Location = new System.Drawing.Point(667, 4);
            this.Btn_Mini.Name = "Btn_Mini";
            this.Btn_Mini.Size = new System.Drawing.Size(75, 22);
            this.Btn_Mini.TabIndex = 1;
            this.Btn_Mini.Text = "Maximizar";
            this.Btn_Mini.UseVisualStyleBackColor = false;
            this.Btn_Mini.Click += new System.EventHandler(this.button3_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(825, 433);
            this.Controls.Add(this.SC_Cuerpo);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Ecatch - StoreDocuments";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.Pn_Controles.ResumeLayout(false);
            this.SC_Cuerpo.Panel1.ResumeLayout(false);
            this.SC_Cuerpo.Panel1.PerformLayout();
            this.SC_Cuerpo.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.SC_Cuerpo)).EndInit();
            this.SC_Cuerpo.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox Txt_Detail_number;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.SplitContainer SC_Cuerpo;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txt_NumeroID;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txt_CuotaM;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txt_CoPago;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txt_iva;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txt_Subtotal;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txt_Total;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
     
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button Btn_Mini;
        private System.Windows.Forms.Label lbl_info;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txt_Radicacion;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txt_Caja;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox txt_CodigoIps;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label lblDocumentos;
        private System.Windows.Forms.Button btnDocSig;
        private System.Windows.Forms.Button btnDocAnt;
        private System.Windows.Forms.Panel Pn_Controles;
        private System.Windows.Forms.Button Btn_Guardar;
        private System.Windows.Forms.ComboBox cb_TipoId;
        private System.Windows.Forms.MaskedTextBox Mask_FechaAtencion;
        private System.Windows.Forms.MaskedTextBox Mask_Fecha;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox txt_Prefijo;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox txt_Numero;
    }
}

